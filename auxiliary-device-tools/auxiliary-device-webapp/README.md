# Auxiliary Device Webapp
This is used by auxiliary devices to connect to the gateway platform and interact with it.

* Uses the gateway-scanner-lite module to scan for gateways that are nearby.
* Once a nearby gateway is found, gets the link graph and displays the entire network information. 
* Allows applications to be deployed using a web interface.