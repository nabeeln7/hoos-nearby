This project contains modules for the following: 

1. Allows computers to join the gateway network as auxiliary devices. This involves scanning for the gateway network 
using the gateway-scanner-lite module and then being able to view the entire network with the frontend webapp.

2. Modules that support application development for the gateway network platform.